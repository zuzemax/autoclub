import Vue from "vue";
import App from "./App.vue";
import { router } from "./router";
import vuetify from "./plugins/vuetify";
import VeeValidate from "vee-validate"
import "roboto-fontface/css/roboto/roboto-fontface.css";
import "@mdi/font/css/materialdesignicons.css";
import store from "./store";

Vue.config.productionTip = false;

Vue.use(VeeValidate);

new Vue({
  router,
  vuetify,
  store,
  render: h => h(App)
}).$mount("#app");
