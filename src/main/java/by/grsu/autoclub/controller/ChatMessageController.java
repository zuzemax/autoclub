package by.grsu.autoclub.controller;

import by.grsu.autoclub.model.document.ChatMessage;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("api/chat")
public class ChatMessageController {
    @MessageMapping
    @SendTo("/topic/chat")
    public ChatMessage update(ChatMessage message) {
        return null;
    }
}
