package by.grsu.autoclub;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AutoclubApplication {

	public static void main(String[] args) {
		SpringApplication.run(AutoclubApplication.class, args);
	}

}
