package by.grsu.autoclub.persistence.document;

import by.grsu.autoclub.model.document.ChatMessage;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface ChatMessageRepository extends MongoRepository<ChatMessage, Long> {
}
