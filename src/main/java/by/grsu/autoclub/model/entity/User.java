package by.grsu.autoclub.model.entity;

import by.grsu.autoclub.model.Role;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Collection;

@Entity
@Table
@Getter
@Setter
public class User {
    @Id
    @Column
    @GeneratedValue
    private Long id;
    @Column(nullable = false, unique = true)
    private String username;
    @Column(nullable = false, unique = true)
    private String email;
    @Column(nullable = false)
    private String password;
    @ElementCollection(targetClass = Role.class)
    @CollectionTable(name = "USER_ROLE",
            joinColumns = @JoinColumn(name = "USER_ID"))
    @Column(name = "ROLE")
    private Collection<Role> roles;

    public User() {
    }

    public User(String username, String email, String password) {
        this.username = username;
        this.email = email;
        this.password = password;
    }
}
