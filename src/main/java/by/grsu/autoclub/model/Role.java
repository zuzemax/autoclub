package by.grsu.autoclub.model;

public enum Role {
    ROLE_USER, ROLE_ADMIN
}
