package by.grsu.autoclub.model.document;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.mongodb.core.mapping.Document;

@Getter
@Setter
@Document
public class ChatMessage {
    private Long id;
    private String username;
    private String text;
}
