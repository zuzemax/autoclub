package by.grsu.autoclub.service.impl;

import by.grsu.autoclub.model.entity.User;
import by.grsu.autoclub.persistence.jpa.UserRepository;
import by.grsu.autoclub.service.UserService;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class UserServiceImpl implements UserService {
    private final UserRepository userRepository;

    @Override
    public User getByUsername(String username) {
        return userRepository.findByUsername(username)
                .orElseThrow(RuntimeException::new);
    }
}
