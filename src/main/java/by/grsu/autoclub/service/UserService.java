package by.grsu.autoclub.service;

import by.grsu.autoclub.model.entity.User;

public interface UserService {
    User getByUsername(String username);
}
